import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import './App.css';

//Se importan los componentes
//Componentes de empleados
import ListaEmpleados from './components/empleado/Lista';
import CrearEmpleado from './components/empleado/Crear';
import EditarEmpleado from './components/empleado/Editar';

//Componentes de solicitudes
import IndexSolicitud from './components/solicitud/Lista';
import CrearSolicitud from './components/solicitud/Crear';
import EditarSolicitud from './components/solicitud/Editar';

//Componente navbar
import NavBarApp from './layouts/navbar';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<NavBarApp />}>
            <Route index element={<ListaEmpleados />} />    
            <Route path='empleados/crear' element={<CrearEmpleado />} />   
            <Route path='empleados/editar/:id' element={<EditarEmpleado />} />   
            <Route path='solicitudes' element={<IndexSolicitud />} />   
            <Route path='solicitudes/crear' element={<CrearSolicitud />} />   
            <Route path='solicitudes/editar/:id' element={<EditarSolicitud />} />   
            <Route path='*' element={<Navigate replace to="/" />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
