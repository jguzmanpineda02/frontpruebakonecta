import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';

function EditarSolicitud() {
    const [empleados, setEmpleados] = useState([]);
    const [codigo, setCodigo] = useState('');
    const [descripcion, setDescripcion] = useState('');
    const [resumen, setResumen] = useState('');
    const [empleado, setEmpleado] = useState([]);
    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        const fetchEmpleados = async () => {
            const result = await axios.get('http://localhost:8080/empleados');
            setEmpleados(result.data.data);
        }
        fetchEmpleados();
    }, []);

    useEffect(() => {
        const fetchEmpleado = async () => {
            const result = await axios.get(`http://localhost:8080/solicitudes/${id}`);
            setCodigo(result.data.data.codigo);
            setDescripcion(result.data.data.descripcion);
            setResumen(result.data.data.resumen);
            setEmpleado(result.data.data.empleado);
        }
        fetchEmpleado();
    }, [id]);

    const enviarFormulario = async (e) => {
        e.preventDefault();
        const result = await axios.put(`http://localhost:8080/solicitudes/${id}`, { codigo, descripcion, resumen, empleado });
        navigate('/solicitudes');
    }
    return (
        <div>
             <div className='row justify-content-center'>
                <div className='col-md-6'>
                    <form onSubmit={enviarFormulario}>
                        <div className='card'>
                            <div className='card-header'>
                                <h1>Nueva solicitud</h1>
                            </div>
                            <div className="form-group col-10 offset-1">
                                <label htmlFor="codigo">Codigo</label>
                                <input type="text" className="form-control" id="codigo" value={codigo} onChange={(e) => setCodigo(e.target.value)} required />
                            </div>
                            <div className="form-group col-10 offset-1">
                                <label htmlFor="descripcion">Descripcion</label>
                                <input type="text" className="form-control" id="descripcion" value={descripcion} onChange={(e) => setDescripcion(e.target.value)} required />
                            </div>
                            <div className="form-group col-10 offset-1">
                                <label htmlFor="resumen">Resumen</label>
                                <input type="text" className="form-control" id="resumen" value={resumen} onChange={(e) => setResumen(e.target.value)} required />
                            </div>
                            <div className="form-group col-10 offset-1 mb-2">
                                <label htmlFor="empleado">Empleado</label>
                                <select className='form-control' value={empleado.id} onChange={(e) => setEmpleado({id: e.target.value})}>
                                    {empleados.map(item =>(
                                        <option value={item.id}> {item.nombre}</option>
                                    ))}                                    
                                </select>
                            </div>
                            <div className='card-footer'>
                                <Link to="/solicitudes" className="btn btn-danger">Cancelar</Link>
                                <button type="submit" className="btn btn-primary">Guardar</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>           
        </div>
    );
}

export default EditarSolicitud;