import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';

function CrearSolicitud() {
    const [empleados, setEmpleados] = useState([]);
    const [codigo, setCodigo] = useState('');
    const [descripcion, setDescripcion] = useState('');
    const [resumen, setResumen] = useState('');
    const [empleado, setEmpleado] = useState([]);
    const [hasErrors, setHasErrors] = useState(false);
    const [errors, setErrors] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchEmpleados = async () => {
            const result = await axios.get('http://localhost:8080/empleados');
            setEmpleados(result.data.data);
        }
        fetchEmpleados();
    }, []);

    const enviarFormulario = async (e) => {
        e.preventDefault();
        const result = await axios.post('http://localhost:8080/solicitudes', { codigo, descripcion, resumen, empleado });
        setHasErrors(result.data.hasErrors);
        if (result.data.hasErrors) {
            setErrors(result.data.errors);
        } else {
            navigate('/solicitudes');
        }
    }


    return (
        <div>
            <div className='container'>
                {hasErrors ? (
                    <div className='row'>
                        <div className='col-md-6 offset-3'>
                            <div className='alert alert-danger'>
                                <ul>
                                    {errors.map(item => (
                                        <li><b>{item.field}: </b> {item.message}</li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                ) : ('')
                }

            </div>
            <div className='row justify-content-center'>
                <div className='col-md-6'>
                    <form onSubmit={enviarFormulario}>
                        <div className='card'>
                            <div className='card-header'>
                                <h1>Nueva solicitud</h1>
                            </div>
                            <div className="form-group col-10 offset-1">
                                <label htmlFor="codigo">Codigo</label>
                                <input type="text" className="form-control" id="codigo" value={codigo} onChange={(e) => setCodigo(e.target.value)} required />
                            </div>
                            <div className="form-group col-10 offset-1">
                                <label htmlFor="descripcion">Descripcion</label>
                                <input type="text" className="form-control" id="descripcion" value={descripcion} onChange={(e) => setDescripcion(e.target.value)} required />
                            </div>
                            <div className="form-group col-10 offset-1">
                                <label htmlFor="resumen">Resumen</label>
                                <input type="text" className="form-control" id="resumen" value={resumen} onChange={(e) => setResumen(e.target.value)} required />
                            </div>
                            <div className="form-group col-10 offset-1 mb-2">
                                <label htmlFor="empleado">Empleado</label>
                                <select className='form-control' onChange={(e) => setEmpleado({id: e.target.value})}>
                                    {empleados.map(item =>(
                                        <option key={item.id} value={item.id}> {item.nombre}</option>
                                    ))}                                    
                                </select>
                            </div>
                            <div className='card-footer'>
                                <Link to="/solicitudes" className="btn btn-danger">Cancelar</Link>
                                <button type="submit" className="btn btn-primary">Guardar</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    );
}

export default CrearSolicitud;