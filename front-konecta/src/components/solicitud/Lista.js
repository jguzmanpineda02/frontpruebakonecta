import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

function ListaSolicitudes() {
    const [solicitudes, setSolicitudes] = useState([]);

    useEffect(() => {
        const fetchSolicitudes = async () => {
            const result = await axios.get('http://localhost:8080/solicitudes');
            setSolicitudes(result.data.data);
        }

        fetchSolicitudes();
    }, []);

    const eliminar = async (id) => {
        await axios.delete(`http://localhost:8080/solicitudes/${id}`);
        const newSolicitud = solicitudes.filter(item => item.id !== id);
        setSolicitudes(newSolicitud);
    }

    return (
        <div className='container'>
            <div className='row'>
                <div className='col-md-12'>
                    <h1>Lista de Solicitudes</h1>
                    <Link to="/solicitudes/crear" className="btn btn-success">Crear Solicitud</Link>
                    <table className="table table-sm table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Descripcion</th>
                                <th>Resumen</th>
                                <th>Empleado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {solicitudes.map(item => (
                                <tr key={item.id}>
                                    <td>{item.codigo}</td>
                                    <td>{item.descripcion}</td>
                                    <td>{item.resumen}</td>
                                    <td>{item.empleado.nombre}</td>
                                    <td>
                                        <Link to={`/solicitudes/editar/${item.id}`} className="btn btn-sm btn-primary mr-2">Editar</Link>
                                        <button onClick={() => eliminar(item.id)} className="btn btn-sm btn-danger">Eliminar</button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default ListaSolicitudes;