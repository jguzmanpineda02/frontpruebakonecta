import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';

function ListaEmpleados() {
    const [empleados, setEmpleados] = useState([]);

    useEffect(() => {
        const fetchEmpleados = async () => {
            const result = await axios.get('http://localhost:8080/empleados');
            setEmpleados(result.data.data);
        }

        fetchEmpleados();
    }, []);

    const eliminar = async (id) => {
        await axios.delete(`http://localhost:8080/empleados/${id}`);
        const newEmpleados = empleados.filter(item => item.id !== id);
        setEmpleados(newEmpleados);
    }

    const mostrarFecha = (fecha_ingreso) =>{
        return moment(fecha_ingreso).format('DD/MM/YYYY h:mm:ss');
    }

    return (
        <div className='container'>
            <div className='row'>
                <div className='col-md-12'>
                    <h1>Lista de Empleados</h1>
                    <Link to="/empleados/crear" className="btn btn-success">Crear empleado</Link>
                    <table className="table table-sm table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Salario</th>
                                <th>Fecha registro</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {empleados.map(item => (
                                <tr key={item.id}>
                                    <td>{item.nombre}</td>
                                    <td>{item.salario}</td>
                                    <td>{mostrarFecha(item.fecha_ingreso)}</td>
                                    <td>
                                        <Link to={`/empleados/editar/${item.id}`} className="btn btn-sm btn-primary mr-2">Editar</Link>
                                        <button onClick={() => eliminar(item.id)} className="btn btn-sm btn-danger">Eliminar</button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default ListaEmpleados;