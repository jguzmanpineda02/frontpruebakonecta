import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';

function EditarEmpleado() {
    const [nombre, setNombre] = useState('');
    const [salario, setSalario] = useState('');    
    const { id } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        const fetchEmpleado = async () => {
            const result = await axios.get(`http://localhost:8080/empleados/${id}`);
            setNombre(result.data.data.nombre);
            setSalario(result.data.data.salario);
        }

        fetchEmpleado();
    }, [id]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        const result = await axios.put(`http://localhost:8080/empleados/${id}`, { nombre, salario });
        navigate('/');
    }

    return (
        <div>
            <div className='container'>                
                <div className='row justify-content-center'>
                    <div className='col-md-6'>
                        <form onSubmit={handleSubmit}>
                            <div className='card'>
                                <div className='card-header'>
                                    <h1>Editar el empleado</h1>
                                </div>
                                <div className="form-group col-10 offset-1">
                                    <label htmlFor="nombre">Nombre</label>
                                    <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => setNombre(e.target.value)} required />
                                </div>
                                <div className="form-group col-10 offset-1 mb-2">
                                    <label htmlFor="salario">Salario</label>
                                    <input type="text" className="form-control" id="salario" value={salario} onChange={(e) => setSalario(e.target.value)} required />
                                </div>
                                <div className='card-footer'>
                                    <Link to="/" className="btn btn-danger">Cancelar</Link>
                                    <button type="submit" className="btn btn-primary">Guardar</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div >
    );
}

export default EditarEmpleado;