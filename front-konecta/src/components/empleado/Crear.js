import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';

function Crear() {
    const [nombre, setNombre] = useState('');
    const [salario, setSalario] = useState('');
    const [hasErrors, setHasErrors] = useState(false);
    const [errors, setErrors] = useState([]);
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();
        const result = await axios.post('http://localhost:8080/empleados', { nombre, salario });
        setHasErrors(result.data.hasErrors);
        if (result.data.hasErrors) {
            setErrors(result.data.errors);
        } else {
            navigate('/');
        }
    }
    

    return (
        <div>
            <div className='container'>
                {hasErrors ? (
                    <div className='row'>
                        <div className='col-md-6 offset-3'>
                            <div className='alert alert-danger'>
                                <ul>
                                    {errors.map(item => (
                                        <li><b>{item.field}: </b> {item.message}</li>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                ) : ('')
                }

            </div>
            <div className='row justify-content-center'>
                <div className='col-md-6'>
                    <form onSubmit={handleSubmit}>
                        <div className='card'>
                            <div className='card-header'>
                                <h1>Nuevo empleado</h1>
                            </div>
                            <div className="form-group col-10 offset-1">
                                <label htmlFor="nombre">Nombre</label>
                                <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => setNombre(e.target.value)} required />
                            </div>
                            <div className="form-group col-10 offset-1 mb-2">
                                <label htmlFor="salario">Salario</label>
                                <input type="number" className="form-control" id="salario" value={salario} onChange={(e) => setSalario(e.target.value)} required />
                            </div>
                            <div className='card-footer'>
                                <Link to="/" className="btn btn-danger">Cancelar</Link>
                                <button type="submit" className="btn btn-primary">Guardar</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    );
}

export default Crear;