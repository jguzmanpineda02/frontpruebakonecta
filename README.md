# Prueba tecnica Konecta
**React Js (JavaScript)**
A continuacion se decribe el paso a paso para el despliegue de la prueba tecnica en entorno local, cabe destacar que esta es la parte front de la prueba.

- Primero se debe tener Node instalado en la maquina
- Luego, se clona el proyeco en el directorio de preferencia
- Después ejecuta el comando **npm install** en una terminal en la raiz del proyecto
- Luego ejecutar el comando **npm start** para correr el proyecto React de forma local.

Y con esto tenemos nuestro proyecto corriendo de manera local.

**NOTA:** Para que este parte funcione de maneja correcta la parte back de la prueba debe estar corriendo en el local.
